package in.dreamplug.dao;

import in.dreamplug.domain.User;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface UserDao  {

    @GetGeneratedKeys
    @SqlUpdate("INSERT INTO users ( external_id, user_name , user_designation , created_at , updated_at) VALUES ( :u.externalId , :u.userName , :u.userDesignation , :u.createdAt , :u.updatedAt);")
    long insert(@BindBean ("u") User user);
}
