package in.dreamplug.dao;

import in.dreamplug.domain.Bug;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import javax.ws.rs.QueryParam;
import java.util.List;

public interface BugDao {

    @GetGeneratedKeys
    @SqlUpdate("INSERT INTO bugs (external_id,bug_name,bug_description, priority, created_at,updated_at) VALUES ( :b.externalId, :b.bugName, :b.bugDescription, :b.priority, :b.createdAt, :b.updatedAt);")
    long insert(@BindBean ("b") Bug bug);

    @SqlUpdate("update bugs set assigned_to = :uid where external_id = :bid ")
    void assign(@Bind("bid")String bugExtId, @Bind("uid") Long userId);

    @SqlQuery("select id, external_id, bug_name, bug_description, priority, assigned_to, created_at, updated_at from bugs where assigned_to = :uid order by <filter> <order>")
    @RegisterBeanMapper(Bug.class)
    List<Bug> getByPriority(@Bind("uid") Long userId, @Define("filter") String filter, @Define("order") String order);

    @SqlQuery("select id, external_id, bug_name, bug_description, priority, assigned_to, created_at, updated_at from bugs where assigned_to = :uid order by priority DESC limit :limit offset :offset")
    @RegisterBeanMapper(Bug.class)
    List<Bug> getPaginated(@Bind("uid") Long userId, @Bind("limit") Long limit, @Bind("offset") Long offset);

    @SqlQuery("select count(id) from bugs where assigned_to = :uid")
    @RegisterBeanMapper(Bug.class)
    Long getcount(@Bind("uid") Long userId);

}
