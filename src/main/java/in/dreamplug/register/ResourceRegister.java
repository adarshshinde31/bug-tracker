package in.dreamplug.register;

import com.google.common.base.Strings;
import com.zaxxer.hikari.HikariDataSource;
import in.dreamplug.configuration.DatabaseConfiguration;
import in.dreamplug.configuration.BugServiceConfiguration;
import in.dreamplug.exception.AppExceptionMapper;
import in.dreamplug.health.DatabaseHealthCheck;
import in.dreamplug.resource.BugResource;
import in.dreamplug.resource.UserResource;
import in.dreamplug.service.BugService;
import in.dreamplug.service.UserService;
import io.dropwizard.setup.Environment;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

@AllArgsConstructor
public class ResourceRegister {

    public void register(final Environment environment, final BugServiceConfiguration configuration) {
        final HikariDataSource dataSource = createDataSource(environment, configuration.getDatabaseConfiguration());
        final Jdbi jdbi = Jdbi.create(dataSource);
        jdbi.installPlugin(new SqlObjectPlugin());
        final BugService bugService = new BugService(jdbi);
        final UserService userService = new UserService(jdbi);
        environment.jersey().register(new BugResource(bugService));
        environment.jersey().register(new UserResource(userService));
        environment.jersey().register(AppExceptionMapper.class);
        environment.healthChecks().register("MYSQL", new DatabaseHealthCheck(dataSource));
    }

    private HikariDataSource createDataSource(Environment environment, DatabaseConfiguration databaseConfig) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(databaseConfig.getUrl());
        dataSource.setUsername(databaseConfig.getUser());
        dataSource.setPassword(databaseConfig.getPassword());
        dataSource.setMinimumIdle(databaseConfig.getMinPoolSize());
        dataSource.setMaximumPoolSize(databaseConfig.getMaxPoolSize());
        dataSource.setIdleTimeout((long)databaseConfig.getMaxIdleTime());
        dataSource.setMaxLifetime((long)databaseConfig.getMaxLifeTime());
        dataSource.setAutoCommit(databaseConfig.isAutoCommit());
        dataSource.setReadOnly(databaseConfig.isReadOnly());
        dataSource.setConnectionInitSql(databaseConfig.getTestStatement());
        dataSource.setConnectionTimeout((long)databaseConfig.getConnectionTimeout());
        if (!Strings.isNullOrEmpty(databaseConfig.getPoolName())) {
            dataSource.setPoolName(databaseConfig.getPoolName());
        }
        dataSource.setMetricRegistry(environment.metrics());
        return dataSource;
    }
}
