package in.dreamplug;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import in.dreamplug.configuration.BugServiceConfiguration;
import in.dreamplug.register.ResourceRegister;
import io.dropwizard.Application;
import io.dropwizard.configuration.ConfigurationSourceProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class bugtrackerApplication extends Application<BugServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new bugtrackerApplication().run(args);
    }

    @Override
    public String getName() {
        return "bugtracker";
    }

    @Override
    public void initialize(final Bootstrap<BugServiceConfiguration> bootstrap) {
        // TODO: application initialization
        ConfigurationSourceProvider provider = new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                new EnvironmentVariableSubstitutor(false));
        bootstrap.setConfigurationSourceProvider(provider);
    }

    @Override
    public void run(final BugServiceConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
        final ResourceRegister resourceRegister = new ResourceRegister();
        resourceRegister.register(environment, configuration);
        environment.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

    }

}
