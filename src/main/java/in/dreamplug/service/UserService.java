package in.dreamplug.service;


import in.dreamplug.dao.UserDao;
import in.dreamplug.domain.User;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

@AllArgsConstructor
public class UserService {

    private Jdbi jdbi;

    public User insert(final  User user){
        user.prePersist();
        final Long id = jdbi.withExtension(UserDao.class, dao -> dao.insert(user));
        user.setId(id);
        return user;
    }
}
