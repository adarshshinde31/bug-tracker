package in.dreamplug.service;

import in.dreamplug.dao.BugDao;
import in.dreamplug.domain.Bug;
import in.dreamplug.domain.PaginatedResponse;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

@AllArgsConstructor
public class BugService {

    private Jdbi jdbi;

    public Bug insert(final  Bug bug){
        bug.prePersist();
        final Long id = jdbi.withExtension(BugDao.class,dao -> dao.insert(bug));
        bug.setId(id);
        return bug;
    }

    //assign bug to a particular user
    public void assignBug(final String bugExtId, final Long userId){
        jdbi.useExtension(BugDao.class,dao -> dao.assign(bugExtId,userId));
    }

    //get list of bugs assigned
    public List<Bug> getBugsPriority(final  Long userId,String filter,String order){
        return jdbi.withExtension(BugDao.class, dao -> dao.getByPriority(userId,filter,order));
    }

    public PaginatedResponse getBugsPaginated(final  Long userId, final Long limit, final Long pagenumber){

        Long offset = (pagenumber-1) * limit;
        Long totalData = jdbi.withExtension(BugDao.class,dao -> dao.getcount(userId));
        PaginatedResponse paginatedResponse = new PaginatedResponse();

        if((totalData - (offset+limit)) > 0)
            paginatedResponse.setNextPage(true);
        else
            paginatedResponse.setNextPage(false);

        paginatedResponse.setBugs(jdbi.withExtension(BugDao.class, dao -> dao.getPaginated(userId, limit,offset)));
        return paginatedResponse;
    }

}
