package in.dreamplug.resource;

import com.codahale.metrics.annotation.Timed;
import in.dreamplug.domain.Bug;
import in.dreamplug.domain.PaginatedResponse;
import in.dreamplug.service.BugService;
import io.dropwizard.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/bugtracker")
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Timed
@AllArgsConstructor
public class BugResource {

    private BugService bugService;

    @POST
    public Response create(final @NotNull @Validated @Valid Bug bug){
        final Bug createBug = bugService.insert(bug);
        return Response.status(Response.Status.CREATED).entity(createBug).build();
    }

    @PATCH
    @Path("/assign")
    public Response assignUser(@QueryParam("bugid") String bugId, @QueryParam("userid") Long userId){
        bugService.assignBug(bugId,userId);
        return  Response.status(Response.Status.CREATED).build();
    }

    @GET
    @Path("/getbuglist")
    public List<Bug> getbugs(@QueryParam("userid") Long userId,@QueryParam("filterby") String filter,@QueryParam("order") String order){
        return bugService.getBugsPriority(userId,filter,order);
    }

    @GET
    @Path("/bugspaginated")
    public PaginatedResponse getBugsPaginated(@QueryParam("userid") Long userId, @QueryParam("buglimit") Long limit, @QueryParam("pagenumber") Long offset){
        return  bugService.getBugsPaginated(userId,limit,offset);
    }

}