package in.dreamplug.resource;


import com.codahale.metrics.annotation.Timed;
import in.dreamplug.domain.Bug;
import in.dreamplug.domain.User;
import in.dreamplug.service.UserService;
import io.dropwizard.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Slf4j
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Timed
@AllArgsConstructor
public class UserResource {

    private UserService userService;

    @POST
    public Response create(final @NotNull @Validated @Valid User user){
        final User createdUser = userService.insert(user);
        return Response.status(Response.Status.CREATED).entity(createdUser).build();
    }
}
