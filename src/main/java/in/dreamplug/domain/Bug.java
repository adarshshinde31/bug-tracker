package in.dreamplug.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import in.dreamplug.Mergeable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Bug extends ExternalBase implements Mergeable<Bug> {

    @NotNull
    @Size (min = 1, max = 45)
    private String bugName;

    @NotNull
    @Size (min = 1, max = 200)
    private  String bugDescription;

    //@NotNull
    private Long priority;

    //@NotNull
    private Long assignedTo;

    @Override
    public void merge(Bug bug) {
        if(StringUtils.isNotBlank(bug.getBugName())){
            this.bugName = bug.getBugName();
        }
        if(StringUtils.isNotBlank(bug.getBugDescription())){
            this.bugDescription = bug.getBugDescription();
        }
        if(bug.getPriority() != null){
            this.priority = bug.getPriority();
        }
        if(bug.getAssignedTo() != null){
            this.priority = bug.getAssignedTo();
        }
    }

}
