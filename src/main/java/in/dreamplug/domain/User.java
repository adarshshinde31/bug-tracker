package in.dreamplug.domain;

import in.dreamplug.Mergeable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User extends ExternalBase implements Mergeable<User> {

    @NotNull
    @Size(min = 0,max = 45)
    private String userName;

    @NotNull
    @Size(min = 0,max = 45)
    private  String userDesignation;

    @Override
    public void merge(User user) {
        if(StringUtils.isNotBlank(user.getUserName())){
            this.userName = user.getUserName();
        }
        if(StringUtils.isNotBlank(user.getUserDesignation())){
            this.userDesignation = user.getUserDesignation();
        }
    }


}
