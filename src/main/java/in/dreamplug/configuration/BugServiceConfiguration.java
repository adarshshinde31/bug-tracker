package in.dreamplug.configuration;

import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BugServiceConfiguration extends Configuration {
    private DatabaseConfiguration databaseConfiguration;
}
