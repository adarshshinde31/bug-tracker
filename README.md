# bugtracker

How to start the bugtracker application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/bugtracker-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

POSTMAN API COLLECTION - https://www.getpostman.com/collections/e1a5e55b0e59c32c8faf

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
